[warning]: /img/Information_red_35x35.png?classes=info "Warning"

Écrire une introduction succincte...

## Le fond, informations techniques
<p><img src="img/LRT_01_01_00056_72dpi.jpg" style="float: right; width: 30%; margin: 10px;"/><img src="img/LRT_01_01_00056_72dpi.jpg" style="float: right; width: 30%; margin: 10px;"/><img src="img/LRT_01_01_00056_72dpi.jpg" style="float: right; width: 30%; margin: 10px;"/></p>

* Type de données : un ensemble de scans de feuillles volantes
* Images : facsimilés, résolution maximale disponible de ?92dpi?


## Transcription, informations techniques
* Type de transcription : pseudo-diplomatique, avec annotation de ???
* Logiciel : [Oxygen](https://www.oxygenxml.com/)
* Format : XML-TEI
* Structuration : un fichier TEI par feuillet
* Schéma ou DTD : <img src="/img/Information_red_35x35.png" class="info"/>

## Méthodoloqie de transcription
* Une personne se voit attribuer un ensemble de facsimilés à transcrire.
* Relecture, validation ?



## Contact :
* Vous êtes transcripteur : ...
* vous voulez en savoir plus sur ce projet : ...



